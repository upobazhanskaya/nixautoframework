package nix.selfmade.autointensive.spec
import geb.spock.GebReportingSpec
import nix.selfmade.autointensive.page.BlogPage
import nix.selfmade.autointensive.page.StartPage

class NixNavigationSpec extends GebReportingSpec {
    def "Navigate to Blog page"(){
        when:
        to StartPage

        and:
        "User navigates to Blog page"()

        then:
        at BlogPage
    }
}
