package nix.selfmade.autointensive.page

import geb.Page

class StartPage extends Page{
    static content = {
        bloglink (wait:true) {$("a", text: "Blog")}

    }

    static at = {
        title.contains("Outsourcing Offshore Software Development Company")
    }
    def "User navigates to Blog page"(){

        bloglink.click()
    }
}
